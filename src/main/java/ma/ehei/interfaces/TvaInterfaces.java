package ma.ehei.interfaces;

public interface TvaInterfaces {
    Double calculeTva(Double montant);
    Double calculeTTC(Double montant);
}
