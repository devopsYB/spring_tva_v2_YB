package ma.ehei.Beans;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import ma.ehei.interfaces.TvaInterfaces;

@Component("ES")

public class ES implements TvaInterfaces {
	@Value("${TVA.ES}")
	private Double ValueTVA;
	public Double calculeTva(Double montant) {
		Double tva = Double.valueOf(0);
		if (montant != null && montant > 100) {
			tva = montant * ValueTVA;
		}
		return tva;
	}

	public Double calculeTTC(Double montant) {
		Double TTC = Double.valueOf(0);
		TTC = calculeTva(montant) + montant;
		return TTC;
	}
}