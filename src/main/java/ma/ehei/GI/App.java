package ma.ehei.GI;

import java.util.Scanner;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import ma.ehei.Configs.AppConfig;
import ma.ehei.Services.Facture;

public class App 
{
    public static void main( String[] args )
    {

		ApplicationContext applicationContext = new AnnotationConfigApplicationContext(AppConfig.class);
        Facture facture = applicationContext.getBean(Facture.class);
        Scanner SC = new Scanner(System.in);
        System.out.println("Entrer la somme de vos achats :");
        Double A = SC.nextDouble();
        System.out.println("La Valeur de TVA est : "+facture.calculeTVA(A)+" DH");
        System.out.println("La Valeur TTC : "+facture.calculeTTC(A)+" DH");
        System.out.println(facture.calculeTTC(A).getClass());
        SC.close();

    }
}
