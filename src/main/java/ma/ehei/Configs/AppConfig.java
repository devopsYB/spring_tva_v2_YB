package ma.ehei.Configs;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@PropertySource("classpath:Text.properties")
@ComponentScan("ma.ehei.Beans")
@ComponentScan("ma.ehei.Services")
@Configuration
public class AppConfig {

}
