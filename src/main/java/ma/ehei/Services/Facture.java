package ma.ehei.Services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import ma.ehei.interfaces.TvaInterfaces;

import org.springframework.stereotype.Component;

@Component

public class Facture {

	@Autowired
	@Qualifier("ES")
	private TvaInterfaces TVA;
	public Facture(@Qualifier("ES")TvaInterfaces A) { // Pour chaque Interface objet One Qualifier need
		this.TVA = A;
	}
	public Double calculeTVA(Double B) {
		return TVA.calculeTva(B);
	}
	public Double calculeTTC(Double C) {
		return TVA.calculeTTC(C);
	}
	
}
